<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Btc extends Model
{
    //

    public function user()
    {
        return $this->belongsTo(User::class,'email', 'email');
    }

    public function pay(){
        return $this->belongsTo(PaymentType::class,'pay_id','id');
    }
}
