<?php
/**
 * Created by PhpStorm.
 * User: mavericks
 * Date: 3/22/18
 * Time: 8:52 AM
 */

namespace App\Helpers;


class Helper
{
    public  static  function addOrdinalSuffix($number){
        if(!in_array(($number % 100), array(11,12,13))){
            switch ($number % 10){
                case 1: return $number."st";
                case 2: return $number."nd";
                case 3: return $number."rd";
            }
        }
        return $number."th";
    }


}