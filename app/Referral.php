<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Referral extends Model
{
    //

    private $count = 0;
    private static $line = 0;
    public static function FindRefLink($referrer,$referred)
    {
        $rf = Referral::where(['referrer' => $referrer,'referred' => $referred])->first();
        //dd($rf);
        return $rf->ref_link;
    }
    public static function FindReferrals($id)
    {
        return Referral::where(['referrer'=>$id])->get();
    }

    public static function FindByUserAndReferrer($id,$referrer)
    {
        $data = Referral::where(['referred' => $id, 'referrer' => $referrer])->first();
        return $data;
    }

    public function user()
    {
        return $this->belongsTo(User::class,'referrer');
    }

    public function direct($id){
        return count(Referral::where('referrer', $id)->get());
    }
    public function indirect($id){
        $ref = Referral::where('referrer', $id)->get();
        $this->line = 1;
        foreach($ref as $r){
            $this->indirectLogic($r);
        }
        return $this->count;
    }

    public function indirectLogic($ref){

        $ref = Referral::where('referrer', $ref->referred)->get();
        if(count($ref) > 0){
            $this->count += count($ref);
            foreach($ref as $r){
                $rr = self::FindReferrals($r->referred);
                if(count($rr) > 0){
                    //$this->count += count($ref);
                    //count = 3;
                    $this->indirectLogic($r);
                }
            }
        }

    }


    private function loger($data){
        Log::info('TEST', (array)$data);

    }
}
