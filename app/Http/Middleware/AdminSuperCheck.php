<?php

namespace App\Http\Middleware;

use App\Helpers\AuthCheck;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminSuperCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(AuthCheck::AuthSuperCheck())
        {
            return $next($request);
        }
        else{
            Auth::logout();
            return redirect()->action('UtilityController@Home');
        }
    }
}
