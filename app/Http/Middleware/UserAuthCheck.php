<?php

namespace App\Http\Middleware;

use App\Helpers\AuthCheck;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserAuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(AuthCheck::AuthUserCheck())
        {
            //dd('Checked');
            return $next($request);
        }
        else{
            //dd('Not');
            Log::info('User with admin right and ID ' . Auth::id() . 'tried to access a user profile');
            Auth::logout();
            return redirect()->action('UtilityController@Home');
        }
    }
}
