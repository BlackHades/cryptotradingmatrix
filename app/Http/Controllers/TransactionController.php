<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    private function getLogger()
    {
        return new Logger();
    }

    public function user()
    {
        return view('User.transaction',['title' =>'Transaction','trans' => Auth::user()->trans()->orderByDesc('created_at')->paginate(30)]);
    }
}
