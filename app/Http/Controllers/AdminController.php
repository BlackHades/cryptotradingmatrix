<?php

namespace App\Http\Controllers;

use App\AcctReq;
use App\AcctType;
use App\Btc;
use App\Comment;
use App\Helpers\AppMailer;
use App\Helpers\Logger;
use App\Helpers\Mailerr;
use App\Helpers\TradeSync;
use App\Info;
use App\Investments;
use App\MainAccount;
use App\PaymentType;
use App\Referral;
use App\RegistrationType;
use App\SchoolFees;
use App\TClass;
use App\Testimonial;
use App\Ticket;
use App\Transaction;
use App\User;
use App\UserInv;
use App\Utility;
use App\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rules\In;

class AdminController extends Controller
{
    public function __construct()
    {
        try{
            TradeSync::Sync();
        }
        catch (\Exception $exception){
            $this->getLogger()->LogError('Error Occured when Syncing Trade', $exception,null);
        }
    }

    private function getId($id)
    {
        return $id/(8009 * 8009);
    }

    private function getLogger()
    {
        return new Logger();
    }

    //User View.
    public function Dashboard()
    {
        return view('Admin.dashboard',['title' => 'Dashboard', 't' => 'users','users' => User::where(['role_id' => 3])->orderByDesc('created_at')->paginate(100),'key' => null, "s" => "Not null"]);
    }

    public function userSearch(Request $request){
       //  dd($request->all());
        $reg = RegistrationType::where('name','LIKE','%'.$request->key.'%')->get();
        $acct = AcctType::where('name','LIKE','%'.$request->key.'%')->first();
        $pay = PaymentType::where('name','LIKE','%'.$request->key.'%')->first();
        $class = TClass::where('name','LIKE','%'.$request->key.'%')->first();
        if($request->key != null) {
            $k = $request->key;
            $query = User::query();
            $query->where('id','LIKE','%'.$k.'%')
                ->orwhere('email','LIKE','%'.$k.'%')
                ->orwhere('fullname','LIKE','%'.$k.'%')
                ->orwhere('r_link','LIKE','%'.$k.'%');
            if(isset($acct)){
                $query->orwhere('acc_id','LIKE','%'. $acct->id . '%');
            }
            if(!empty($reg)){
                foreach ($reg as $r){
                    $query->orwhere('reg_type','LIKE','%'. $r->id . '%');
                }
            }
            if(isset($pay)){
                $query->orwhere('payment_id','LIKE','%'. $pay->id . '%');
            }
            if(isset($class)){
                $query->orwhere('class_id','LIKE','%'. $class->id . '%');
            }
            $query->where('role_id','=',3);
            return view('Admin.dashboard',['title' => "Search $k",'users' =>  $query->paginate(100), 't' => 'users', 'key' => $k,'s' => "notnull"]);
        }
        return redirect()->back();
    }

    public function userAdmin(Request $request){
        $id = decrypt($request->id);
        $user = User::find($id);
        if(isset($user)){
            $user->role_id = 2;
            $user->r_link = null;
            $user->save();
            Session::flash('success',"Operation completed successfully");
        }else{
            Session::flash("error", "User cannot Be found");
        }
        return redirect()->back();
        //dd($request->id);
    }
    public function UserView($id)
    {
        $u = User::find(decrypt($id));
        $a = $u->acct();
        return view('Admin.user_view',['title' => 'User View','user' =>  $u,'acct' => $a]);
    }
    public function UserEdit(Request $request, $id, Mailerr $mailerr)
    {
        //dd($request->all());
        $this->validate($request,[
            'password' => 'required'
        ]);
        try{
            $u = User::find($this->getId($id));
            $old = $u;
            if(Hash::check($request->password, Auth::user()->password)) {
                $u->acc_id = $request->acc_id != null ? $request->acc_id : $u->acc_id;
                $u->reg_type = $request->reg_type;
                if ($u->class_id == $request->class_id) {
                    $u->class_id = $request->class_id;
                } else {
                    $u->class_id = $request->class_id;
                    $u->r_mark = 0;
                }
                //$u->class_id = $request->class_id == null ? 0 : $request->class_id;
                $u->is_active = (bool)$request->is_active;
                $u->activated = (bool)$request->activated;
                if ($u->activated) {
                    if (Referral::FindByUserAndReferrer($u->id, $u->referrer) == null) {
                        User::MultiGenRef($u->id);
                    }
                    $u->start_date = Carbon::now();
                }
                $u->save();
                Session::flash('success', 'User Profile Updated Succuessfully');
                Log::info('User Profile Updated', ['old_user' => $old, 'new_user' => $u]);
            }
            else{
                Session::flash('error','Incorrect Password');
            }
        }
        catch(\Exception $ex)
        {
            Session::flash('error','Unable to Update User Profile');
            $this->getLogger()->LogError('admin: User Profile Update','Unable to Update User Profile',$ex,['old_user' => $old,'new_user' => $u], $mailerr);
        }
        return redirect()->back();
    }
    public function UserAction($id,$aid, Mailerr $mailerr)
    {
        $op = decrypt($aid);
        $user = User::find(decrypt($id));
        if($op == 1)
        {

        }
        if($op == 2)
        {
            //dd($user->trans()->orderByDesc('created_at'));
            return view('Admin.trans',['title' => 'Transactions','trans' => $user->trans()->orderByDesc('created_at')->get()]);
        }
        if($op == 3)
        {
            return view('Admin.withdrawal',['title'=>'Withdrawal Request','with' => $user->withd()->orderByDesc('created_at')->get()]);
        }

        if($op == 4)
        {
            try{
                $t = new TradeSync();
                $t->Sync();
            }
            catch (\Exception $exception){
                $this->getLogger()->LogError('Error Occured when Syncing Trade', $exception,null, $mailerr);
            }
          return view('Admin.tradings',['title' => 'Tradings','trades' => $user->Trade()->orderByDesc('created_at')->get()]);
        }

        if($op == 5)
        {
            return view('Admin.account',['title' => 'Accounts','main' => $user->bal()->orderBy('created_at','DESC')->get()]);
        }
    }

    //admin
    public function Admin()
    {
        return view('Admin.admin',['title'=>'admin','admin' =>  User::where('role_id','=' ,2)->get()]);

    }
    public function AdminPost(Request $request, Mailerr $mailerr)
    {

        $this->validate($request, [
            'fullname' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'conf_password' => 'required|same:password'
        ],['conf_password.same' => 'Passwords Mismatch']);
        //dd($request->all());
        $user = new User();
        $user->fullname = $request->fullname;;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
       // $user->phone_number = 'empty';
        $user->role_id = 2;
        $user->is_active = true;
        $user->activated = true;
        try{
            $user->save();
            Session::flash('success','Administrator Added Successfully');
            Log::info('Add admin: admin has been added',['admin' => $user, 'by' => Auth::user()]);
            return redirect()->back();
        }
        catch(\Exception $ex)
        {
            $this->getLogger()->LogError(' admin Registration Error: Unable to register admin',$ex,['User' => $user], $mailerr);
            Session::flash('error','Oops, An Error Occurred and We Could Not Complete the request, Please Try Again');
            return redirect()->back();
        }
    }
    public function AdminDelete($id, Mailerr $mailerr)
    {
        $id = decrypt($id);
//        if(Auth::user()->role_id == 1)
//        {
//
//            try{
//                $adm = User::where(['role_id' => 2, 'id' => $id])->first();
//                $adm->is_active = false;
//                $adm->save();
//                Session::flash('success','Operation was successfully carried out');
//                Log::info('Delete admin: admin Successfully Deleted. ', ['admin' => $adm,'Super_admin' => Auth::id()]);
//            }
//            catch(\Exception $ex)
//            {
//                $this->Logger()->LogError('Delete admin: Unable to delete admin',$ex,['admin' => $adm]);
//                Session::flash('error','Unable to carry out operation. Please check log files');
//            }
//        }
//        else{
//            Session::flash('error','You don\'t have the necessary permission to carry out this operation.');
//            Log::error('Tyring to Delete an admin with out proper access', ['admin' => $id, 'by' => Auth::id()]);
//        }
        try{
            $adm = User::where(['role_id' => 2, 'id' => $id])->first();
            $adm->is_active = false;
            $adm->save();
            Session::flash('success','Operation was successfully carried out');
            Log::info('Delete admin: admin Successfully Deleted. ', ['admin' => $adm,'Super_admin' => Auth::id()]);
        }
        catch(\Exception $ex)
        {
            $this->getLogger()->LogError('Delete admin: Unable to delete admin',$ex,['admin' => $adm],$mailerr);
            Session::flash('error','Unable to carry out operation. Please check log files');
        }
        return redirect()->back();
    }

    //Mail
    public function Mail()
    {
      return view('Admin.mail',['title' => 'Mail','user' => null,'active' => null]);
    }
    public function MailAll()
    {
        $user = User::where('role_id', 3)->pluck('email');
        //dd($user);
        $a = [];
        $s = null;
        foreach ($user as $m)
        {
            if($s == null)
            {
                $s = trim($m);
            }
            else{
                $s = trim($s) . ',' . trim($m);
            }
        }
        //dd([$s][0]);
        $user = $s;
        return view('Admin.mail',['title' => 'Mail','user' => $user,'active' => "all"]);
    }
    public function MailSingle($email)
    {
        $user = User::where('email',decrypt($email))->first();
        return view('Admin.mail',['title' => 'Mail','user' => trim($user->email), 'active' => null]);
    }
    public function MailActive(){
        $user = User::where(['activated' => true, 'is_active' => true, 'role_id' => 3])->pluck('email');
        //dd($user);
        $a = [];
        $s = null;
        foreach ($user as $m)
        {
            if($s == null)
            {
                $s = trim($m);
            }
            else{
                $s = trim($s) . ',' . trim($m);
            }
        }
        //dd([$s][0]);
        $user = $s;
        return view('Admin.mail',['title' => 'Mail','user' => $user, 'active' => 'active']);
    }
    public function MailInactive(){
        $user = User::where(['activated' => 0, 'role_id' => 3])->pluck('email');
        $user2 = User::Where(['is_active' => 0, 'role_id' => 3])->pluck('email');
        $user->toBase()->merge($user2);
        //dd($user, count($user), $user2, count($user));
        $a = [];
        $s = null;
        foreach ($user as $m)
        {
            if($s == null)
            {
                $s = trim($m);
            }
            else{
                $s = trim($s) . ',' . trim($m);
            }
        }
        //dd([$s][0]);
        $user = $s;
        return view('Admin.mail',['title' => 'Mail','user' => $user, 'active' => 'inactive']);
    }
    public function MailSend(Request $request, Mailerr $mailer)
    {
        $this->validate($request,[
            'to' => 'required',
            'msg' => ' required'
        ]);

        Log::info('startt');

        try{
            $emails = $request->to[0];
            $em = explode(',',$emails);
            foreach ($em as $e){
                $msg = $request->msg;
                $sub = $request->subject;
                $mailer->sendRawMail(trim($e),$msg,$sub);
                Session::flash('success',"Email Sent Successfully.");
                Log::info("Email sent to $e");
            }
        }
        catch(\Exception $ex){
            Session::flash('error','an Error Occurred, Please Try Again');
            $this->getLogger()->LogError('Unable To send mail',$ex,['Emails' => $emails,'message' => $msg,'subject' => $sub], $mailer);
        }
        return redirect()->back()->withInput();
    }

    //Trading
    public function Trade()
    {
        return view('Admin.tradings',['title' => 'Tradings','trades' => Investments::orderBy('created_at','DESC')->get()]);
    }
    public function TradeAction($id, $a_id, Mailerr $mailerr)
    {

        try{
            $a = decrypt($a_id);
            $i = Investments::FindbyInvD(decrypt($id));
            $old = $i;
            if($a == 1)
            {
                $i->ts_id = 6;
                $i->start_date = Carbon::now();
                $i->save();
                $t = new Transaction();
                $t->t_id = Transaction::GenerateTID();
                $t->user_id = $i->user_id;
                $t->amount = $i->amount;
                $t->descn = 'Trade - ' . $i->inv_id;
                $t->tn_id = 1;
                $t->t_type = 2;
                $t->ts_id = 1;
                if($t->save())
                {
                    try {
                        if(!UserInv::findById($i->user_id))
                        {
                            Investments::ReferralBonusTeacher($i, $i->amount);
                            UserInv::dataS($i->user_id, true);
                        }
                    }
                    catch(\Exception $ex) {
                        $this->getLogger()->LogError('an Error Occurred When Giving Inv Bonus',$ex,['inv' => $i], $mailerr);
                    }
                    Log::info('Transaction saved',['Trans' => $t]);
                }
                else{
                    Session::flash('error','data could not be saved');
                }
                //Put In Transaction
            }

            if($a == 2)
            {
                $i->ts_id = 5;
                $i->save();
            }
            $i->save();
            Log::info('Operation completed successfully',['old' =>$old,'i' => $i,'action' => $a,'by' =>Auth::id()]);
            Session::flash('success','Operation Completed Successfully');
        }
        catch(\Exception $ex){
            $this->getLogger()->LogError('Trade Action: An Error Occurred',$ex,['by' =>Auth::id()], $mailerr);
            Session::flash('error','Oops An Error Occurred, Please Try Again');
        }
        return redirect()->back();

    }

    //Transaction
    public function Transaction()
    {
        return view('Admin.trans',['title' => 'Transactions','trans' => Transaction::orderBy('created_at', 'DESC')->get()]);
    }


    //Account
    public function Account()
    {
        return view('Admin.account',['title' => 'Accounts','main' => MainAccount::orderBy('created_at','DESC')->get()]);
    }
    public function AccountUpdate($id)
    {
        return view('Admin.account_update',['title' => 'Update Account','Main' => MainAccount::find(decrypt($id))]);
    }
    public function AccountUpdatePost(Request $request, $id, Mailerr $mailerr)
    {
        $this->validate($request,[
           'amount' => 'required|numeric',
           'acct_type' => 'required',
            'password' => 'required'
        ],[
            'acct_type.required' => 'Please Select An Account Type To Update.'
        ]);

        $main = MainAccount::find(decrypt($id));
        if($request->acct_type == 1)
        {
            $main->trade_bal = $main->trade_bal + $request->amount;
        }

        if($request->acct_type == 2)
        {
            $main->ref_bal = $main->ref_bal + $request->amount;
        }

        try{
           if(Hash::check($request->password, Auth::user()->password))
           {
               $main->save();
               Session::flash('success','Account Updated Successfully');
               return redirect()->action('AdminController@Account');
           }
           else{
               Session::flash('error','Incorrect Password. Please Try Again');
               return redirect()->back();
           }
        }
        catch(\Exception $ex)
        {
            $this->getLogger()->LogError('An error Occurred When Updating Account', $ex,['Main'=>$main,'req' => $request->all()], $mailerr);
            Session::flash('error','An Error Occurred. Please Try Again');
            return redirect()->back();
        }

    }

    //Referrals
    public function Referrals()
    {
        return view('Admin.referrals',['title' => 'Referrals','ref' => Referral::orderByDesc('created_at')->get()]);
    }


    //Tickets
    public function Ticket()
    {
        return view('Admin.ticket',['title'=>'Tickets','tick' => Ticket::orderByDesc('updated_at')->get()]);
    }
    public function TicketComment($id)
    {
        $ticket = Ticket::find(decrypt($id));
        return view('Admin.ticket_comment',['title'=>'Tickets','ticket' => $ticket,'comments' => $ticket->comments()->orderBy('created_at','ASC')->get()]);
    }
    public function TicketCommentPost(Request $request, Mailerr $mailerr)
    {
        $this->validate($request,[
            'message' => 'required'
        ]);
        //dd($request->all());
        try{
            $c = new Comment();
            $c->user_id = Auth::id();
            $c->ticket_id = $request->ticket_id;
            $c->comment = $request->message;
            if($c->save())
            {
                Session::flash('success','Message Successfully saved.');
                if ($c->ticket->user->id !== Auth::user()->id) {
                    $t = Ticket::find($c->ticket->id);
                    $t->res = true;
                    //$t = Ticket::find($c->ticket->);
                    $t->save();
                    $mailerr->sendTicketComments($c->ticket->user, Auth::user(), $c->ticket, $c);

                }
            }
            else{
                Session::flash('error','An error occurred when sending comment');
            }
            return redirect()->back();

        }
        catch(\Exception $ex)
        {
            //dd($ex);
            $this->getLogger()->LogError('An Error Occured When Sending Message',$ex,['comment' =>  $c], $mailerr);
            Session::flash('error','An Error Occured, Please Try Again');
            return redirect()->back();
        }
    }


    //Request
    public function Request()
    {
        $t = AcctReq::where('resolved',0)->get();
        $chk = true;
        if(count($t) < 1)
            $chk = false;
        else
            $chk = true;

        return view('Admin.acct_req',['chk' => $chk,'title' => 'Requests','req' => AcctReq::orderByDesc('created_at')->get()]);
    }
    public function ReqRes($id, Mailerr $mailerr)
    {
        //dd(decrypt($id));
        try{
            $a = AcctReq::find(decrypt($id));
            $user = User::find($a->user_id);
            $user->reg_type = $a->new_acct_type;
            $user->save();
            $a->resolved = true;
            $a->save();
            Session::flash('success','Request Resolved Successfully');
            Log::info('request resolved', ['by' => Auth::id(), 'Req' => $a]);
        }
        catch(\Exception $ex)
        {
            Session::flash('error','Unable to resolve request.');
            $this->getLogger()->LogError('Error Occurred When trying to resolve request.', $ex, ['by' => Auth::id(), 'Req' => $a], $mailerr);

        }

        return redirect()->back();
    }

    //----------------------------Entry Fee------------------------------------
    public function Payment()
    {
        return view('Admin.payment',['title' => 'Payments','sf' => SchoolFees::orderByDesc('created_at')->get()]);
    }
    public function PaymentResolved($id, Mailerr $mailerr)
    {
        //dd($id);
        try{
            $s = SchoolFees::find(decrypt($id));
            $s->resolved = true;
            $s->save();
            Session::flash('success','Request Resolved Successfully.');
        }
        catch(\Exception $ex)
        {
            Session::flash('error', 'An Error Occurred. Please Try Again Later');
            $this->getLogger()->LogError('Unable TO Resolve Request',$ex, ['r' => $s], $mailerr);
        }
        return redirect()->back();
    }

    //----------------------------Payment Type------------------------------------
    public function payType()
    {
        return view('Admin.btc',['title' => 'BTC','btc'=> Btc::orderByDesc('created_at')->get()]);
    }

    public function payTypePost(Request $request, Mailerr $mailerr)
    {
        //dd($request->all());
        $this->validate($request,[
            'btc' => 'required',
            'paytype' => 'required'
        ]);
        try{
            $s = new Btc();
            $s->address = $request->btc;
            $s->pay_id = $request->paytype;
            $s->save();
            Session::flash('success','Request Completed Successfully.');
        }
        catch(\Exception $ex)
        {
            //dd($ex);
            Session::flash('error', 'An Error Occurred. Please Try Again Later');
            $this->getLogger()->LogError('Unable TO Resolve Request',$ex, ['r' => $s], $mailerr);
        }

        return redirect()->back();
    }
    public function payTypeDelete(Request $request){
        $id = decrypt($request->id);
        $btc = Btc::find($id);
        if(isset($btc)){
            $btc->delete();
        }
        Session::flash("success",'BTC Address deleted Successfully');
        return redirect()->back();
    }


    //----------------------------Info------------------------------------
    public function Info(Request $request)
    {
        return view('Admin.info',['title' => ' Information', 'info' => Info::orderByDesc('created_at','DESC')->paginate(1), 'id'=> $request->id != null ? decrypt($request->id) : null]);
    }
    public function InfoID(Request $request)
    {
        //dd($request->id);
        return view('Admin.info',['title' => ' Information', 'info' => Info::orderByDesc('created_at','DESC')->get(), 'id'=> $request->id != null ? decrypt($request->id) : null]);
    }

    public function InfoPost(Request $request)
    {
        $this->validate($request,[
            'user_id' => 'required',
            'message' => 'required',
            'priority' => 'required',
        ]);
        //dd($request->all());
        $info = new Info();
        $info->user_id = $request->user_id;
        $info->message = $request->message;
        $info->priority = $request->priority;
        $info->read_count = 0;
        if($info->save())
        {
            Session::flash('success','Message Sent');
        }
        else{
            Session::flash('error','Unable To Send Message');
        }
        return redirect()->back();
    }


    //----------------------------Testimonial------------------------------------
    public function testimonial(){
        return view('Admin.testimonial', ['title' => 'Testimonial','test' => Testimonial::all()]);
    }

    public function testimonialPost(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'word' => 'required',
            'image' => 'required|image'
        ]);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $test = new Testimonial();
            $test->name = $request->name;
            $test->word = $request->word;
            $filename = time() . "-".$test->name.".".$file->getClientOriginalExtension();
            $file->move(public_path("uploads"), $filename);
            $test->image = $filename;
            if($test->save()){
                Session::flash('success', "Testimonial Saved.");
            }else{
                Session::flash('error','Testimonial Could Not Be Saved');
            }
            return redirect()->back();
        }else{
            Session::flash('error','Image Not Found');
            return redirect()->back();
        }


    }

    public function testimonialDelete(Request $request){
        $id = decrypt($request->id);
        $test = Testimonial::find($id);
        if(isset($test)){
            $test->delete();
        }
        Session::flash("success",'Testimonial deleted Successfully');
        return redirect()->back();
    }
}