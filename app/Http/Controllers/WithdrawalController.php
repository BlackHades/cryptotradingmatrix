<?php

namespace App\Http\Controllers;

use App\Helpers\Mailerr;
use App\MainAccount;
use App\Transaction;
use App\Utility;
use App\Withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class WithdrawalController extends Controller
{
    //Users
    public function user()
    {
        $user = Auth::user();
        if(Utility::find(1)->value != 1)
        {
            Session::flash('error','Withdrawals Can\'t Be Made At This Time');
            return redirect()->back();
        }

        if(Auth::user()->acct == null || $user->acct->name == null || $user->acct->btc == null)
        {
            Session::flash('warning','Kindly update your wallet details at the account section before applying for a withdrawal.');
        }

        return view('User.with',['title' => 'Withdrawals','with'=>Withdrawal::where('user_id',Auth::id())->orderBy('created_at','DESC')->paginate(20),
            'inv' => Transaction::where(['user_id' =>  Auth::id(),'tn_id' => 4,'t_type' => 1])->orWhere(['user_id' =>  Auth::id(),'tn_id' => 5,'t_type' => 1])->where('tn_id','<>',2)->paginate(20), 'a' => Auth::user()->bal, 'acct' => Auth::user()->acct]);//Come back
    }
    public function WithPost(Request $request, Mailerr $mailerr)
    {
        $uu = Auth::user();
        //dd($request->all());
        if(Auth::user()->acct == null || $uu->acct->name == null || $uu->acct->btc == null)
        {
            Session::flash('error','Kindly update your wallet details at the account section before applying for a withdrawal.');
            return redirect()->back();
        }
        $this->validate($request,[
            'amount' => 'required|numeric'
        ]);
        //dd($request->all(), $request->id);
        $tr = Transaction::find(decrypt($request->id));
        //dd($id, decrypt($id));
        if(isset($request->id) && (decrypt($request->id) == "main" || decrypt($request->id) == "ref")){
            if((decrypt($request->id) == "main" && Auth::user()->bal->trade_bal < $request->amount) || (decrypt($request->id) == "ref" && Auth::user()->bal->ref_bal < $request->amount))
            {
                Session::flash('error','Insufficient Balance. ');
                return redirect()->back();
            }
            $w = new Withdrawal();
            $w->w_id = Withdrawal::GenerateWID();
            $w->t_id = decrypt($request->id) == "main" ? "Main Account" : "Referral Account";
            $w->ts_id = 3;
            $w->amount = $request->amount;
            $w->user_id = Auth::id();
            try{
                if($w->save())
                {
                    $mailerr->notify("Withdrawal request");
                    Session::flash('success','Withdrawal Request Submitted Successfully');
                    Log::info('Withdrawal Request Created',['with' => $w,'trans' => $tr]);
                }
                else
                {
                    Session::flash('error','An Error Occurred. Please try again later.');
                    Log::error('An Error Occurred when saving Withdrawal Request',['with' => $w,'trans' => $tr]);
                }

            }
            catch(\Exception $ex){
                $this->getLogger()->LogError('An Error Occurred When Trying to create withdrawal request', $ex,['with' => $w,'trans' => $tr]);
                Session::flash('error','An Error Occurred When Creating the Withdrawal Request. Please Try Again');
            }
        }
        return redirect()->back();
    }









    //Admin
    public function admin()
    {
        return view('Admin.withdrawal',['title'=>'Withdrawal Request','with' => Withdrawal::orderBy('created_at','DESC')->get()]);
    }
    public function WithAction($id,$a_id, Mailerr $mailerr)
    {
        try{
            $a = decrypt($a_id);
            $i = Withdrawal::find(decrypt($id));
            $old = $i;
            if(decrypt($a_id) == 1)
            {
                //dd($a, $i);
                $temp = false;
                if(strtolower($i->t_id) == "main account"){
                    //dd('main');
                    $temp = MainAccount::updateTradeBal($i->user_id, -($i->amount));
                }elseif (strtolower($i->t_id) == "referral account"){
                    //dd('ref');
                    $temp = MainAccount::updateRefBal($i->user_id, -($i->amount));
                }else{
                    $temp = false;
                    Session::flash('error', 'Invalid account balance type');
                    return redirect()->back();
                }
                if($temp){
                    $i->ts_id = 1;
                    $i->save();
                    $t = new Transaction();
                    $t->t_id = Transaction::GenerateTID();
                    $t->user_id = $i->user_id;
                    $t->amount = $i->amount == null ? $i->trans->amount : $i->amount;
                    $t->descn = 'Trade-' . $i->w_id;
                    $t->tn_id = 2;
                    $t->t_type = 2;
                    $t->ts_id = 1;
                    $t->save();
                    Log::info('Transaction saved',['Trans' => $t]);
                    //Put In Transaction
                }else{
                    Session::flash('error', 'A Minor Error Occurred');
                }
            }

            if($a == 2)
            {
                $i->t_id = "Withdrawal from $i->t_id - Declined";
                $i->ts_id = 5;
                $i->save();
            }
            $i->save();
            Log::info('Operation completed successfully',['old' =>$old,'i' => $i,'action' => $a,'by' =>Auth::id()]);
            Session::flash('success','Operation Completed Successfully');
        }
        catch(\Exception $ex){
            $this->getLogger()->LogError('With Action: An Error Occurred',$ex,['old' =>$old,'i' => $i,'action' => $a,'by' =>Auth::id()], $mailerr);
            Session::flash('error','Oops An Error Occured, Please Try Again');
        }

        return redirect()->back();
    }
}
