<?php

namespace App\Http\Controllers;

use App\Helpers\Logger;
use App\Helpers\Mailerr;
use App\Investments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class TradeController extends Controller
{
    private function getLogger()
    {
        return new Logger();
    }
    //user
    public function trade()
    {
        if(Auth::user()->reg_type == 1)
        {
            Session::flash('error','This Account can\'t Be Used For Trading. Kindly Visit The Personal Details Page to Upgrade Your Account');
            return redirect()->back();
        }
        else
            return view('User.invest',['title' => 'Investment','inv' => Auth::user()->inv()->orderBy('created_at','DESC')->get()]);
    }
    public function makeTrade(Request $request, Mailerr $mailerr)
    {
        $this->validate($request,[
            'duration' => 'required|numeric',
            'amount' => 'required|numeric',
            'pay_type' => 'required',
        ]);

        if($request->amount < 10000)
        {
            Session::flash('error','The Minimum Amount For Trading CTM10,000.00');
            return redirect()->back();
        }
        if($request->duration < 1)
        {
            Session::flash('error','The Minimum Trading Duration Is 1 Month');
            return redirect()->back();
        }

        $i = new Investments();
        $i->inv_id = Investments::generateInv();
        $i->user_id = Auth::id();
        $i->amount = $request->amount;
        $i->profit = 0;
        $i->month_count = 0;
        $amt = $request->amount;

        if($amt >= 10000 && $amt <= 100000)
            $i->irate = 10;
        if($amt > 100000 && $amt <= 200000)
            $i->irate = 12;
        if($amt > 200000 && $amt <= 400000)
            $i->irate = 14;
        if($amt > 400000 && $amt <= 600000)
            $i->irate = 16;
        if($amt > 600000 && $amt <= 800000)
            $i->irate = 18;
        if($amt > 800000 && $amt <= 1000000)
            $i->irate = 20;
        if($amt > 1000000 && $amt <=2000000)
            $i->irate = 25;
        if($amt > 2000000)
            $i->irate = 30;

        $i->duration = $request->duration;
        $i->ts_id = 3;
        //dd($i);
        try {
            //dd($request->all());
            if ($i->save()) {
                $mailerr->trading($i->user->email, explode(' ', $i->user->fullname)[0], $request->pay_type, $i->id);
                $mailerr->notify("Investment Request");
                Session::flash('success', 'Trading Application Pending Authorization');
                Log::info('Investment Successful', ['by' => Auth::id(), 'Inv' => $i]);
                return redirect()->route('user_wtdn', ['id' => encrypt($i->id)]);
            } else {
                Session::flash('error', 'An error occurred. Please try again later.');
                Log::info('Unable to save trading', ['by' => Auth::id(), 'Inv' => $i]);
                return redirect()->back();
            }

        }
        catch(\Exception $ex)
        {
            $this->getLogger()->LogError('Trading Application error',$ex,['by' => Auth::id(),'Inv' => $i]);
            Session::flash('error','An error occured. Please Try Again');
        }
        //$this->WTDN($i);
        return redirect()->back();
    }

}
