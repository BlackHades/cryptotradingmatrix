<?php

namespace App\Http\Controllers;

use App\Referral;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReferralsController extends Controller
{

    //Admin Call
    public function adminFindReferrals(Request $request){
        $user_id = null;
        $ref_id = null;
        $id = explode('ASDFGHJKL', $request->id);
        //dd($request->id);
        if(count($id) > 1){
            $user_id = decrypt($id[0]);
            $ref_id = decrypt($id[1]);
            $gen = $id[2];
            //dd($user_id. $ref_id);
        }else{
            $user_id = decrypt($request->id);
            $ref_id = $user_id;
            $gen = 1;
            //dd($user_id);
        }
       // $user_id = ;
        $ref = Referral::where('referrer',$ref_id)->get();
        //dd($ref);
        return view('Admin.dashboard',['title' => 'Dashboard', 't' => 'referrals','user' => User::find($user_id),'ref' =>  $ref,'key' => null, "s" => "Not null",'gen' => $gen,'ref_id' => $ref_id]);
    }


    //User Call
    public function referrals()
    {
        return view('User.referral',['title' => 'Referrals','ref' => Auth::user()->ref()->orderBy('created_at','DESC')->get()]);
    }
    public function refOthers($id)
    {
        //$user
        return view('User.referral',['title' => User::find(decrypt($id))->fullname . ' Referrals','ref' => User::find(decrypt($id))->ref()->orderBy('created_at','DESC')->get()]);
    }
}
