<?php

namespace App\Http\Controllers;

use App\AcctDetails;
use App\AcctReq;
use App\FileEntry;
use App\Helpers\Logger;
use App\Helpers\Mailerr;
use App\Investments;
use App\Referral;
use App\SchoolFees;
use App\Transaction;
use App\User;
use App\Utility;
use App\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    private function getLogger()
    {
        return new Logger();
    }

    //

    public function Dashboard()
    {
        return view('User.dashboard',['title' => "Dashboard", 'user' =>Auth::user()]);
    }


    //Profile
    public function Profile()
    {
        return view('User.profile',['title' => 'Profile','user' => Auth::user()]);
    }
    public function ProfileEdit(Request $request)
    {
        $this->validate($request,[
            'fullname' => 'required',
            'reg_type' => 'required',
            'pay_type' => 'required'
        ],
        [   'fullname.required' => 'Full Name Field Is Required',
            'reg_type.required' => 'Registration Type Field is Required',
            'pay_type.required' => 'Payment Type Field is Required'
        ]);
        //dd($request->all());
        $user = User::find(Auth::id());
        $old_user = $user;
        $user->fullname = $request->fullname;
        $user->reg_type = $request->reg_type;
        $user->payment_id = $request->pay_type;
        try{
            $user->save();
            Session::flash('success','Profile Updated Successfully');
            Log::info('Profile Update Successful',['old-user' => $old_user,'new-user' => $user,'by' => Auth::id()]);
        }
        catch(\Exception $ex)
        {
            $this->getLogger()->LogError('ERROR Profile Update Error',$ex, ['old-user' => $old_user,'new-user' => $user,'by' => Auth::id()]);
            Session::flash('error','An Error Occurred, Please Try Again');
        }
        return redirect()->back();
        //validate and save

    }
    public function ProfileEditPassword(Request $request)
    {
        $this->validate($request,[
            'password' => 'required',
            'new_password' => 'required',
            'new_confirm_password' => 'required|same:new_password'
        ], [
            'password.required' => 'Old Password Field Is Required',
            'new_password.required' => 'New Password Field Is Required',
            'new_confirm_password.required' => 'Confirm New Password Field Is Required',
            'new_confirm_password.same' => 'New Pasword Mismatch'
        ]);

        $user = User::find(Auth::id());
        if(Hash::check($request->password, $user->password))
        {
            $user->password = Hash::make($request->new_password);

            try{
                $user->save();
                Session::flash('success','Password Successfully Changed');
            }
            catch(\Exception $ex){
                Session::flash('error','An Error Occurred, Please Try Again Later');
                $this->getLogger()->LogError('Password Change Error', $ex);
            }
            //dd($request->all());
        }
        else{
            Session::flash('error','Password Does Not Match');
        }

        return redirect()->back();

    }
    public function AccountUpgrade(Request $req, Mailerr $mailer)
    {
        //dd($req->all());
        if(!AcctReq::FindByID(Auth::id()))
        {
            $m = new AcctReq();
            $m->user_id = Auth::id();
            $m->old_acct_type = Auth::user()->acc_id;
            $m->new_acct_type = $req->new_reg_type;
            $m->resolved = false;
            try{
                $m->save();
                $mailer->notify("Account Request");
                Session::flash('success','Your Account Upgrade Request Has Been Successfully Submitted. We Will Get Back To You Shortly');
            }
            catch (\Exception $ex)
            {
                $this->getLogger()->LogError('An Error Occurred when tryig to save Request',$ex,['reg' => $m,'by' => Auth::id()]);
                Session::flash('error', 'An Error occured, Please try Again');
            }
        }
        else{
            Session::flash('error','You Still Have A request Pending. Please Let that Request Be Resolved First.');
        }
        return redirect()->back();
        //Session::flash()
    }


    //Account
    public function Account()
    {
        //dd( Auth::user()->bal,  Auth::user()->acct, Auth::user(), Auth::id());
        return view('User.account',['title' => 'Accounts','a' => Auth::user()->bal, 'acct' => Auth::user()->acct]);
    }
    public function AccountPost(Request $request)
    {
        try{
            //dd($request->all(), $request->btc);
            $u = AcctDetails::where(['user_id' => Auth::id()])->first();
            if(!isset($u))
            {
                $a = new AcctDetails();
                $a->user_id = Auth::id();
                $a->bank = $request->bank;
                $a->name = $request->wallet_name;
                $a->number = $request->number;
                $a->btc = $request->wallet_address;
                $a->save();
            }
            else{
                $u->bank = $request->bank;
                $u->name = $request->wallet_name;
                $u->number = $request->number;
                $u->btc = $request->wallet_address;
                $u->save();
            }
            Session::flash('success','Account Details Updated Successfully');
        }catch(\Exception $ex){
            $this->getLogger()->LogError('Account Could Not Be Updated', $ex,['acc' => $request->all()]);
            Session::flash('error','An Error Occurred. Please try again later.');
        }
        return redirect()->back();
    }

    public function WTDN($id)
    {
        $i = Investments::find(decrypt($id));
        return view('User.wtdn',['title'=>'What To Do Next', 'i' => $i]);
    }
    public function EOP($token)
    {
        if(decrypt($token) == 1 || decrypt($token) == 2)
        {
            return view('User.EOP',['title' => 'Upload Evidence Of Payment','t' => decrypt($token)]);
        }
        else
        {
            Session::flash('error','Invalid Method Of Payment');
            return redirect()->back();
        }
    }
    public function EOPP($token, Request $request, Mailerr $mailerr)
    {
        try{
            //dd($request->all());
            if(decrypt($token) == 1)
            {
                $this->validate($request,[
                    'pop' => 'required|image',
                    'hash' => 'required',
                ]);
                if($request->hasFile('pop'))
                {
                    $n = new SchoolFees();
                    $n->email = $request->email;
                    $n->for = "Trading Fees";
                    $n->pay_type = decrypt($token);
                    $n->hash_id = $request->hash;
                    $fl = new FileEntry();
                    $file = $request->file('pop');
                    $imagename = $n->email .'-'.Carbon::now()->timestamp . '.' . $file->getClientOriginalExtension();
                    Storage::disk('uploads')->put( $imagename,  File::get($file));
                    $fl->mime = $file->getClientMimeType();
                    $fl->original_filename = $file->getClientOriginalName();
                    $fl->filename = $imagename;
                    try{
                        $fl->save();
                        $n->pop = $imagename;
                        $n->save();
                        $mailerr->notifyUserOfPayment($request->email,2);
                        $mailerr->notify("Payment Request");
                        Log::info('File Entry And Trade Fees Evidence Saved.',['School' => $n,'FileEntry' => $fl]);
                        Session::flash('success','File Submitted Successfully. We Will Get Back To You Shortly');
                    }
                    catch(\Exception $ex)
                    {
                        //dd($ex);
                        Session::flash('error','An Error Occurred. Please Try Again');
                        $this->getLogger()->LogError('Unable To Save File or School Fees',$ex,['School' => $n,'FileEntry' => $fl], $mailerr);
                    }
                }
            }

            if(decrypt($token) == 2)
            {
                $this->validate($request,[
                    'email' => 'required',
                    'teller' => 'required',
                ]);
                if($request->hasFile('teller'))
                {
                    $n = new SchoolFees();
                    $n->email = $request->email;
                    $n->for = "Trading Fees";
                    $n->pay_type = decrypt($token);
                    $fl = new FileEntry();
                    $file = $request->file('teller');
                    $imagename = $n->email .'-'.Carbon::now()->timestamp . '.' . $file->getClientOriginalExtension();
                    Storage::disk('uploads')->put( $imagename,  File::get($file));
                    $fl->mime = $file->getClientMimeType();
                    $fl->original_filename = $file->getClientOriginalName();
                    $fl->filename = $imagename;
                    //dd($request->all(), decrypt($token), $fl, $n);
                    try{
                        $fl->save();
                        $n->teller = $imagename;
                        $n->save();
                        $mailerr->notifyUserOfPayment($request->email,2);
                        $mailerr->notify("Payment Request");
                        Log::info('File Entry And Trading Fees Evidence Saved.',['School' => $n,'FileEntry' => $fl]);
                        Session::flash('success','File Submitted Successfully. We Will Get Back To You Shortly');
                    }
                    catch(\Exception $ex)
                    {
                        //dd($ex);
                        Session::flash('error','An Error Occurred. Please Try Again');
                        $this->getLogger()->LogError('Unable To Save File or School Fees',$ex,['School' => $n,'FileEntry' => $fl]);
                    }
                }
            }
            return redirect()->back();
        }
        catch (\Exception $ex)
        {
            Session::flash('error','An Error Occured. Please Try Again');
            $this->getLogger()->LogError('An Error Occurred When Opening This Page', $ex, null);
        }
    }
    public function Support()
    {
        return redirect()->action('TicketController@UserTickets');
    }
}
