@extends('master')
@section('body')
    <!-- about -->
    <div class="services">
        <div class="container">
            <div class="w3layouts_header">
                <p><span><i class="fa fa-info-circle" aria-hidden="true"></i></span></p>
                <h5>How <span>It Works</span></h5>
            </div>


            <div class="w3layouts_skills_grids">
                <div class="col-lg-2"></div>
                <div class="col-md-8 agileinfo_about_left text-justify">
                    <div class="w3-card-4 remake">
                        <div class="w3-container w3-center" style="margin-top: 10px">
                            <div class="col-lg-12">
                                <h5 class="header pull-left">We Are Crypto Traders and Network Marketers</h5>
                            </div>
                            <br/>
                            <br/>
                            <p class="card-body text-left text-justify remake">
                                The First group Designed for Supporting one the largest community-based Cryptocurrencies and network marketing(CTM). We offer safe   and anticipated entries, chart analyses, trading lessons, trading insights,trading investment step by step guidance into trading
                            </p><br/>
                        </div>
                    </div>

                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="w3layouts_skills_grids">
                <div class="col-lg-2"></div>
                <div class="col-md-8 agileinfo_about_left text-justify">
                    <h4>One Time Payment Of CTM10,000 To Get Access To 4 Services Such As:</h4>
                    <ul class="list-group text-justify">
                        <li class="list-group-item"><i class="fa fa-hand-o-right"></i>Matrix plan with universal link( company forced matrix to make up to $5,000) - matrix referral commission when you use your referral link you will earn from level1 to level7 deep.</li>
                        <li class="list-group-item"><i class="fa fa-hand-o-right"></i> Free Crypto trading, online training (learn how to buy altcoins and when to enter market).</li>
                        <li class="list-group-item"><i class="fa fa-hand-o-right"></i> Free Crypto signal through your email address.</li>
                        <li class="list-group-item"><i class="fa fa-hand-o-right"></i> Trading investment( lending package with accurate % monthly ). </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="container">
            <div class="w3layouts_header">
                <p><span><i class="fa fa-info-circle" aria-hidden="true"></i></span></p>
                <h5>Matrix <span>Plan</span></h5>
            </div>
            <div class="w3layouts_skills_grids">
                <div class="col-lg-2"></div>
                <div class="col-md-8 agileinfo_about_left" >
                    {{--<h2 style="font-size:35px;font-family:cursive;color:green;margin-bottom:15px;">OUR MATRIX PLAN</h2>--}}
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Level One Matrix Plan
                                        <span class="glyphicon glyphicon-chevron-right pull-right"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="card">
                                        <div class="card-header">
                                            <p class="text-center">LEVEL ONE</p>
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    CTM10,000 one time payment
                                                </li>
                                                <li class="list-group-item">
                                                    3 automatic referral needed
                                                </li>
                                                <li class="list-group-item">
                                                    CTM 3,300 level1 matrix universal bonus pay out.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Level Two Matrix Plan
                                        <span class="glyphicon glyphicon-chevron-right pull-right"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <div class="card">
                                        <div class="card-header">
                                            <p class="text-center">LEVEL TWO</p>
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    9 automatic referrals needed
                                                </li>
                                                <li class="list-group-item">
                                                    CTM 10,000 Level2 universal bonus pay out
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Level Three Matrix Plan
                                        <span class="glyphicon glyphicon-chevron-right pull-right"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <div class="card">
                                        <div class="card-header">
                                            <p class="text-center">LEVEL THREE</p>
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    27 automatic referrals needed
                                                </li>
                                                <li class="list-group-item">
                                                    CTM 3,300,000 level3 universal bonus pay out                                                    <div class="pull-right">$255</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>
        <br/>
        <br/>
        <div class="container">
            <div class="w3layouts_header">
                <p><span><i class="fa fa-info-circle" aria-hidden="true"></i></span></p>
                <h5>Trading <span>Investment</span></h5>
            </div>
            <div class="w3layouts_skills_grids">
                <div class="col-lg-2"></div>
                <div class="col-md-8 agileinfo_about_left remake">
                    <br/>

                    <ul class="list-group remake">
                        <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM10,000  to CTM100,000 = 10% profit monthly</li>
                        <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM101,000 to CTM200,000= 12% profit monthly</li>
                        <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM201,000 to CTM400,000 =14% profit monthly</li>
                        <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM401,000 to CTM 600,000= 16% monthly</li>
                        <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM601,000 to CTM800,000 =18% monthly</li>
                        <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM801,000 to CTM1,000,000 = 20% monthly</li>
                        <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM1,100,000 to CTM2,000,000 = 25% monthly</li>
                        <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM2,100,000 to unlimited = 30% monthly</li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>

        <br/>
        <br/>
        <br/>
        <div class="container">
            <div class="w3-card-4 remake">
                <br/>
                <div class="w3layouts_header">
                    <p><span><i class="fa fa-info-circle" style="color: yellow;" aria-hidden="true"></i></span></p>
                    <h5 class="remake">HOW OUR REFERRAL COMMISSION AND TEACHER(MANAGER) BONUS WORKS?</h5>
                </div>
                <div class="w3layouts_skills_grids">
                    <div class="col-lg-2"></div>
                    <div class="col-md-8 agileinfo_about_left text-justify">
                        <hr>
                        <h4 class="remake">How To Become Teacher(Manager) And What Is The Manager Bonus?</h4>
                        <hr>
                        <h6 class="remake">How to Become Teacher(Manager)?</h6>
                        <p class="remake">you can become CTM teacher(Our Manager) when you have 20 downlines in your  team.</p>
                        <hr>
                        <h6 class="remake">What's The Manager bonus?</h6>
                        <p class="remake">Once you become manager you will receive a manager bonus in both matrix plan and trading plan:</p>
                        <ul class="list-group">
                            <li class="list-group-item remake">
                                Manager Matrix Bonus: you will a receive a referral commission of  your team in the matrix plan form level 1 to level 7: 10%, 3%, 2%, 2%, 1%, 1%, 0.5% per each members of your team from first generation downlines to seventh generation downlines.
                            </li>
                            <li class="list-group-item remake">
                                Manager trading bonus are same from level1 to level7 according to the amount your team have invested in automatic trading packages
                            </li>
                        </ul>
                        <hr>
                        <h4 class="remake">What's the referral commission and who can earn referral commission?</h4>
                        <ul class="list-group">
                            <li class="list-group-item remake">
                                Referral commission is when you use your referral link to invite members then the participants register and upgrade his/her account to level 1 using your referral code.
                            </li>
                            <li class="list-group-item remake">
                                Every members of the platform can earn referral commission both in matrix and trading, students earn once (on a generation)...and teachers earn different amount on all generations. From level 1 to level 7 in the trading package and entry fee package.
                            </li>
                           </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
@endsection