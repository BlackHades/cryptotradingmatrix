@extends('master')
@section('body')
    <!-- about -->
    <div>
        <div class="services">
            <div class="container remake">
                <br>
                <div class="w3layouts_header">
                    <p><span><i class="fa fa-info-circle" style="color: yellow;" aria-hidden="true"></i></span></p>
                    <h5>Brief description <span>about us</span></h5>
                </div>
                <div class="w3layouts_skills_grids">
                    <div class="col-lg-2"></div>
                    <div class="col-md-8 agileinfo_about_left">
                        <h4 class="text-justify remake">
                            Founded in 7/2017 by three members: Crypto trader , network marketers and cyber security engineers, CTM is the premier  WORLD.-based Crypto currency education platform, providing lightning-reliable trade execution, dependable digital season and industry-leading trading security practices. Our mission is to help advance the blockchain and Crypto industry by fostering innovation, incubating new and emerging technology, and driving transformative change.                        </h4>
                        <br>
                        <br/>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!-- //about -->
        <!-- team -->

        <section class="container text-justify" style="margin-top:70px;">
            <header class=" text-center" style="font-size:25px;color:green;;font-family:cursive">Frequently Asked Questions</header>
            <hr class="hr-dark hm-red-light">
            <div class="row">
                <div class="col-lg-12 w3-card" style="margin: 20px;">
                    <h4 class="card-title">Who Can Join CTM?</h4>
                    <hr>
                    <div class="w3-container card-body">
                        <p><i class="fa fa-arrow-right"></i>
                            Any person of legal age can join  CTM community.</p>
                    </div>
                </div>
                <div class="col-lg-12 w3-card remake" style="margin: 20px;">
                    <h4 class="card-title">How do I register with CTM?</h4>
                    <hr>
                    <div class="w3-container card-body">
                        <p>
                            <i class="fa fa-arrow-right"></i>
                            To be part of our community,you will register on the official website  <a href="http://www.Cryptotradingmatrix.com">http://www.Cryptotradingmatrix.com</a> click on register button on the upper and fill the form then submit
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 w3-card" style="margin: 20px;">
                    <h4 class="card-title">How to subscribe for our 4 tools services ?</h4>
                    <hr>
                    <div class="w3-container card-body">
                        <p>
                            <i class="fa fa-arrow-right"></i>
                            To subscribe for our 4 tools services click on  register on our website fill the registration form, you will receive an email from the company, check your email,copy the wallet address or bank details then make the payment and upload your prove of payment
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 w3-card remake" style="margin: 20px;">
                    <h4 class="card-title">How To Become Teacher(Manager) And What Is The Manager Bonus?</h4>
                    <hr>
                    <div class="w3-container card-body">
                        <p><i class="fa fa-arrow-right"></i> How to Become Teacher(Manager)? - you can become CTM teacher(Our Manager) when you have 20 downlines in your  team.</p>
                        <p>
                            <i class="fa fa-arrow-right"></i>
                            What Is The Manager Bonus? - once you become manager you will receive a manager bonus in both matrix plan and trading plan: <br/>
                            Manager Matrix Bonus: you will a receive a referral commission of  your team in the matrix plan form level 1 to level 7: 10%, 3%, 2%, 2%, 1%, 1%, 0.5% per each members of your team from first generation downlines to seventh generation downlines.
                            <br/> Manager trading bonus are same from level1 to level7 according to the amount your team have invested in automatic trading packages
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 w3-card remake" style="margin: 20px;">
                    <h4 class="card-title">What's the referral commission and who can earn referral commission?</h4>
                    <hr>
                    <div class="w3-container card-body">
                        <p>
                            <i class="fa fa-arrow-right"></i>
                            Referral commission is when you use your referral link to invite members then the participants register and upgrade his/her account to level 1 using your referral code.
                        </p>
                        <p>
                            <i class="fa fa-arrow-right"></i>
                            Every members of the platform can earn referral commission both in matrix and trading, students earn once (on a generation)...and teachers earn different amount on all generations. From level 1 to level 7 in the trading package and entry fee package.
                        </p>
                        </div>
                </div>
                <div class="col-lg-12 w3-card remake" style="margin: 20px;">
                    <h4 class="card-title">How to invest in trading investment?</h4>
                    <div class="w3-container card-body">
                        <p>
                            <i class=" fa fa-arrow-right"></i>
                            Login to your dashboard and click to trade now,input the amount you want to deposit,write the number of month you wish your money should be traded Then wait for your profit
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 w3-card" style="margin: 20px;">
                    <h4 class="card-title">How to write and upload  my testimonials letter?</h4>
                    <div class="w3-container card-body">
                        <p>
                            <i class="fa fa-arrow-right"></i>
                            As soon as you get profit from any of our 4 tools services, everyone has to write and upload his/her testimonial letter.
                            <br>write your text with:<br>
                        </p>
                        <ul class="list-group">
                            <li class="list-group-item">Your status(ordinary,matrix, signal trader,  ordinary trader investment, participant, manager or admin, . ...)</li>
                            <li class="list-group-item">Date of your subscription package</li>
                            <li class="list-group-item">Investment amount package(for automatic trader)</li>
                            <li class="list-group-item">Amount earned or percentage earned</li>
                            <li class="list-group-item">Date of your pay day</li>
                            <li class="list-group-item">Any additional relevant information</li>
                            <li class="list-group-item">Attach a screen shot or a file with confirmation of the amount earned </li>
                            <li class="list-group-item">Upload the link of your video</li>
                            <li class="list-group-item">Login to your dashboard and open ticket of testimonials letter upload your documents and click on send</li>
                        </ul>
                        <p>
                            <strong>Note:</strong> We like to see our members upload their testimonial letters to prove others members that we are paying and working so hard for your success and when any participants of our platform always upload his/her testimonial letters ,we will promote him/her membership to the next level bonus
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 w3-card remake" style="margin: 20px;">
                    <h4 class="card-title">What are the investment opportunities in Crypto trading matrix platform?</h4>
                    <div class="w3-container card-body">
                        <p>
                            <i class="fa fa-arrow-right"></i>
                            This are the following investment opportunities on the CTM platform:<br/>
                        </p>
                        <ul class="list-group">
                            <li class="list-group-item remake">Matrix Plan</li>
                            <li class="list-group-item remake">Trading Signal Subscription</li>
                            <li class="list-group-item remake">Trading Education and Training</li>
                            <li class="list-group-item remake">Trading Investment Subscription</li>
                        </ul>
                        <p>
                            <i class="fa fa-arrow-right"></i>
                            Here is the package of trading investment<br><br/>
                        </p>
                        <ul class="list-group">
                            <ul class="list-group remake">
                                <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM10,000  to CTM100,000 = 10% profit monthly</li>
                                <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM101,000 to CTM200,000= 12% profit monthly</li>
                                <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM201,000 to CTM400,000 =14% profit monthly</li>
                                <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM401,000 to CTM 600,000= 16% monthly</li>
                                <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM601,000 to CTM800,000 =18% monthly</li>
                                <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM801,000 to CTM1,000,000 = 20% monthly</li>
                                <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM1,100,000 to CTM2,000,000 = 25% monthly</li>
                                <li class="list-group-item remake"><i class="fa fa-hand-o-right"></i> CTM2,100,000 to unlimited = 30% monthly</li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </section>


        <script defer src="js/jquery.flexslider.js"></script>
        <script type="text/javascript">
            $(window).load(function(){
                $('.flexslider').flexslider({
                    animation: "slide",
                    start: function(slider){
                        $('body').removeClass('loading');
                    }
                });
            });
        </script>
        <!-- //flexSlider -->
        <!-- stats -->
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.countup.js"></script>
        <script>
            $('.counter').countUp();
        </script>
    </div>
@endsection