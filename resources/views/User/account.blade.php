@extends('master')
@section('body')
    <div class="container " style="margin-top:50px;margin-bottom:100px;">
        <div class="col-md-offset-2 col-md-7 ">
            @include('Partials._message')
            <table class="table table-responsive table-striped table-success">
                <thead>
                <th>S/N</th>
                <th>Trade Account(CTM)</th>
                <th>Referral Account(CTM)</th>
                <th>Updated</th>
                </thead>
                <tbody>
                <tr class="success">
                    <td>1</td>
                    <td>{{$a == null ? "0.00" : $a->trade_bal}}</td>
                    <td>{{$a == null ? "0.00" : $a->ref_bal}}</td>
                    <td>{{$a == null ? Auth::user()->created_at : \Carbon\Carbon::parse($a->updated_at)}}</td>
                </tr>
                </tbody>
            </table>
            <hr/>
            <h4 style="margin-bottom:10px;font-size:29px;" class="text-center text-green">Update Account Details</h4>
            <form method="POST" action="{{route('user_acct_post')}}" id="Regform">
                {{csrf_field()}}
                <label for="reg_type">Wallet Name: </label>
                <div class="input-group form-group col-md-12">
					<span class="input-group-addon">
					</span>
                    <input class="form-control input input-lg" name="wallet_name" id="wallet_name" placeholder="Enter Wallet Name Here E.g ETH" value = "{{$acct != null ? $acct->name : ''}}">
                </div>
                <label for="reg_type">Wallet Address: </label>
                <div class="input-group form-group col-md-12">
					<span class="input-group-addon">
						<span class="fa fa-bitcoin">
						</span>
					</span>
                    <input class="form-control input input-lg" name="wallet_address" id="wallet_address" placeholder="Enter Wallet Address Here" value = "{{$acct != null ? $acct->btc : ''}}">
                </div>
                <div class="form-group  col-md-12 input-group">
                    <input type="submit" class="btn btn-block btn-lg"value="Update"/>
                </div>
            </form>
        </div>
    </div>
@endsection