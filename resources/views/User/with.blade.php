@extends('master')
@section('body')
    <div class="container-fluid jumbotron" style="background-color: white;">
        <p class="well-sm lead">
        <div class="row">
            <div class="col-lg-3">

            </div>
        </div>
    </div>
    <!--// brief information about us ends here-->

    <div class="container" style="margin-top:15px;">
        <h4 class="container-fluid text-center" style="margin-top:5px;margin-bottom:10px;font-size:29px;color:greenyellow">WITHDRAWALS</h4>
        <hr/>
        @include('Partials._message')
        <h4>Withdraw From Account: </h4>
        <table class="table table-responsive table-striped table-success">
            <thead>
            <th>S/N</th>
            <th>Account(CTM)</th>
            <th>Amount(CTM)</th>
            <th>Updated</th>
            <th>Actions</th>
            </thead>
            <tbody>
            <tr class="success">
                <td>1</td>
                <td>Main Account:</td>
                <td>{{$a == null ? "0.00" : $a->trade_bal}}</td>
                <td>{{$a == null ? Auth::user()->created_at : \Carbon\Carbon::parse($a->updated_at)}}</td>
                <td>
                    <form method="post" class="form-inline" action="{{route('user_with_trans', ['id' => encrypt("main")])}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input class="form-control" required style="margin-top: 0" type="number" placeholder="Withdrawal Amount" name="amount" id="amount">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success"><span class="fa fa-money"></span> Withdraw Funds</button>
                        </div>
                    </form>
                </td>
            </tr>
            <tr class="success">
                <td>2</td>
                <td>Referral Account:</td>
                <td>{{$a == null ? "0.00" : $a->ref_bal}}</td>
                <td>{{$a == null ? Auth::user()->created_at : \Carbon\Carbon::parse($a->updated_at)}}</td>
                <td>
                    <form method="post" class="form-inline" action="{{route('user_with_trans', ['id' => encrypt("ref")])}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input class="form-control" required style="margin-top: 0" type="number" placeholder="Withdrawal Amount" name="amount" id="amount">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success"><span class="fa fa-money"></span> Withdraw Funds</button>
                        </div>
                    </form>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    @if($with != null)
        <div class="row" style="margin-bottom: 300px;">
            <div class="container">
                <hr/>
                <h4 class="container-fluid text-center" style="margin-top:15px;margin-bottom:10px;font-size:29px;color:greenyellow">WITHDRAWAL HISTORY</h4>
                <hr/>
                <br/>
                <table class=" col-lg-12 table table-responsive text-center">
                    <thead>
                    <th>S/N</th>
                    <th>Date</th>
                    <th>Withdrawal ID</th>
                    <th>From</th>
                    <th>AMOUNT(CTM)</th>
                    <th>STATUS</th>
                    </thead>
                    <tbody>
                    <?php $i = 1?>
                    @foreach($with as $in)
                        <tr class="
                                @if($in->ts_id == 1|| $in->ts_id == 7)
                                alert-success
                                @elseif($in->ts_id == 3 || $in->ts_id == 6)
                                alert-warning
                                @elseif($in->ts_id == 5 || $in->ts_id == 4 || $in->ts_id == 2)
                                alert-danger
                                @endif">
                            <td>{{$i++}}</td>
                            <td>{{\Carbon\Carbon::parse($in->created_at)}}</td>
                            <td>{{$in->w_id}}</td>
                            <td>{{$in->t_id}}</td>
                            <td>{{$in->amount}}</td>
                            <td>{{$in->status->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="w3-center">
            {{$with->links()}}
        </div>
    @else
        <div class="jumbotron text-center">
            <p>No Withdrawals Yet</p>
        </div>
    @endif
@endsection