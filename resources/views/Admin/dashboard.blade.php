@extends('adminMaster')
@section('body')
    <div class="content-inner">
        <!-- Page Header-->
        <header class="page-header">
            <div class="container-fluid">
                <h2 class="no-margin-bottom"></h2>
            </div>
            @if($t == "users")
                <div class="row">
                    <div class="col-lg-12" style="margin-left: 10px;">
                        <h2>Users</h2>
                        <br/>
                        <hr>

                    </div>
                    <div class="col-lg-6" style="margin-left: 10px;">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        <h3 style="font-size: 16px"><strong>Total Users:</strong> </h3>
                                        <h1>{{\App\User::floorUsers()}}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        <h5 style="font-size: 16px"><strong>Active Users:</strong> </h5>
                                        <h1>{{\App\User::activeUsers()}}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        <h5 style="font-size: 16px"><strong>Inactive Users:</strong> </h5>
                                        <h1>{{\App\User::inactiveUsers()}}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <form action="{{route('searchUser')}}" method="post" class="form-inline">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" required style="margin-right: 5px;" class="form-control" name="key" id="key" placeholder="Search User Here..." value="{{ isset($key) ? $key : ""}}"/>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                <a href="{{route('admin_dashboard')}}" class="btn btn-link">All Users</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-4">
                    @include('Partials._message')
                    <div class=" table table-responsive">
                        <table id="table" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1?>
                            @foreach($users as $user)
                                @if($user->role_id == 3)
                                    <tr class="alert alert-{{$user->activated == false ? 'danger' : 'info'}}">
                                        <td>{{$i++}}</td>
                                        <td>{{$user->fullname}}</td>
                                        <td>{{$user->email}}</td>
                                        <td style="display: inline-flex;">
                                            <a href="{{route('admin_user_view',['id' => encrypt($user->id)])}}" class="btn btn-outline-info btn-sm" style="margin-right: 10px;" data-toggle="tooltip" title="View User"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('admin_info',['id' => encrypt($user->id)])}}" class="btn btn-outline-success btn-sm" style="margin-right: 10px;" data-toggle="tooltip" title="Send Message"><i class="fa fa-comment"></i></a>
                                            <a href="{{route('mail.single',['id' => encrypt($user->email)])}}" class="btn btn-outline-primary btn-sm" style="margin-right: 10px;" data-toggle="tooltip" title="Send Mail"><i class="fa fa-envelope-square"></i></a>
                                            <a href="{{route('user.referrals',['id' => encrypt($user->id)])}}"  class="{{!$user->activated ? "disabled":""}} btn btn-outline-warning btn-sm" style="margin-right: 10px;" data-toggle="tooltip" title="View Referrals"><i class="fa fa-handshake-o"></i></a>
                                            {{--                                        <a href="{{route('admin.make_admin',['id' => encrypt($user->id)])}}" onclick="return confirm('Are you sure you want to make this user an Admin? You can\'t undo this action');" class="btn btn-outline-warning btn-sm" style="margin-right: 10px;"><i class="fa fa-user-circle"></i></a>--}}
                                            {{--<a href="{{route('deleteUser',['id' => encrypt($user->id)])}}" onclick="return confirm('Are you sure you want to delete this User?');" class="btn btn-outline-danger btn-sm" style="margin-right: 10px;"><i class="fa fa-trash"></i></a>--}}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="myCenter">
                        {{$s == null ? ' ' : $users->links()}}
                    </div>
                </div>
            @endif

            @if($t == "referrals")
                <div class="row">
                    <div class="col-lg-12" style="margin-left: 10px;">
                        <h2>{{$user->fullname}}</h2>
                        <hr>
                    </div>
                    <div class="col-lg-5" style="margin-left: 10px;">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card text-center">
                                    <div class="card-header">
                                        <h3 style="font-size: 16px"><strong> Direct Referrals:</strong> </h3>
                                        <h1>{{ (new \App\Referral())->direct($user->id)}}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card text-center">
                                    <div class="card-header">
                                        <h5 style="font-size: 16px"><strong>Indirect referrals:</strong> </h5>
                                        <h1>{{ (new \App\Referral())->indirect($user->id)}}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <form action="{{route('searchUser')}}" method="post" class="form-inline">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" required style="margin-right: 5px;" class="form-control" name="key" id="key" placeholder="Search User Here..." value="{{ isset($key) ? $key : ""}}"/>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                <a href="{{route('admin_dashboard')}}" class="btn btn-link">All Users</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-4">
                    @include('Partials._message')
                    @if(count($ref) > 1)
                        <div class="card">
                            <div class="card-body">
                                <p class="text-center" style="font-size: large; font-style: italic;">{{\App\Helpers\Helper::addOrdinalSuffix($gen). " Generation"}}</p>
                                <p class="text-center" style="font-size: large; font-style: italic;">Referred by: <a href="{{route('admin_user_view',['id' => encrypt($ref_id)])}}" class="btn btn-primary btn-sm">{{\App\User::find($ref_id)->fullname."(".\App\User::find($ref_id)->email.")"}}</a></p>
                                @php($gen++)
                            </div>
                        </div>
                        <div class=" table table-responsive">
                            <table id="table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1?>
                                @foreach($ref as $r)
                                    <?php $ref = \App\User::find($r->referred);?>
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{\Carbon\Carbon::parse($user->created_at)}}</td>
                                        <td>{{$ref->fullname}}</td>
                                        <td>{{$ref->email}}</td>
                                        <td><a href="{{route('user.referrals',['id' => encrypt($user->id)."ASDFGHJKL". encrypt($ref->id)."ASDFGHJKL".($gen)])}}" class="btn btn-success"> <i class="fa fa-eye"></i> View Referrals </a> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="jumbotron-fluid">
                            <div class="justify-content-md-center">
                                <p class="text-center" style="font-size: 50px">No Referrals</p>
                            </div>
                        </div>
                    @endif
                </div>
            @endif
        </header>
    </div>
@endsection