@extends('adminMaster')
@section('body')
    <div class="content-inner">
        <!-- Page Header-->
        <header class="page-header">
            <div class="container-fluid">
                <h2 class="no-margin-bottom"></h2>
            </div>
            <div class="col-lg-8">
                &nbsp;&nbsp;&nbsp;<p style="font-size: xx-large"> <i class=" fa fa-credit-card"></i> Payment Type</p>
            </div>
            <br/>
            <br/>
            <div class="col-lg-4">
                <h2 style="text-align: center;"> Add Payment Type</h2>
                <hr>
                <form action="{{route('admin_pay_type_post')}}" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="label" for="type">Address:</label>
                        <textarea type="text" class="form-control" autofocus name="btc" id="btc" rows="3" required placeholder="Address"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="label" for="type">Type:</label>
                        <select class="form-control" name="paytype" id="paytype">
                            @php($s = \App\PaymentType::all())
                            @foreach($s as $ss)
                                @if($ss->id > 1)
                                    <option value="{{$ss->id}}">{{$ss->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-save"></i> Save</button>
                </form>
            </div>
            <hr/>
            <br/>
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-4" style="margin-left: 3px;">
                    @include('Partials._message')
                    @if(count($btc) > 0)
                        <div class=" table table-responsive">
                            <table id="table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Date</th>
                                    <th>Address</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1?>
                                @foreach($btc as $a)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{\Carbon\Carbon::parse($a->created_at)->toDateString()}}</td>
                                        <td>{{$a->address}}</td>
                                        <td>{{$a->pay->name}}</td>
                                        <td><a href="{{route('admin.pay_type.delete', ['id' => encrypt($a->id)])}}" class="btn btn-outline-danger"><span class="fa fa-trash"></span></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                    @endif
                </div>
                <div class="col-lg-1"></div>

            </div>

        </header>
        <!-- Page Footer-->


    </div>
@endsection